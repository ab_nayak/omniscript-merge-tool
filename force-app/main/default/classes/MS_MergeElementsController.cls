public without sharing class MS_MergeElementsController {

    public static Set<String> setOfNameOfDestinationOSElements = new Set<String>();
    public static String  CONSTANT_DYNAMIC_QUERY      = '';
    public static String  CONSTANT_FIELD_NAMES        = ' Id, Name, vlocity_cmt__SubType__c, vlocity_cmt__Type__c, vlocity_cmt__IsProcedure__c,vlocity_cmt__Version__c,vlocity_cmt__IsActive__c ';
    public static String  CONSTANT_QUERY_CONDITION    = ' where (Id =:searchText OR Name like :searchText OR  vlocity_cmt__Type__c like :searchText OR vlocity_cmt__SubType__c like :searchText ) ';
    public static Integer CONSTANT_MAXIMUM_DML_COUNT  = 145;
    
    /* Source omniscript */
     @AuraEnabled
    public static  List<vlocity_cmt__OmniScript__c>  getAllOmniscriptComponents(String searchText,Boolean destinationQuery){
        if(!(searchText instanceof Id)){
        searchText ='%' +String.escapeSingleQuotes(searchText)+'%';

        }

       if(destinationQuery){
          CONSTANT_QUERY_CONDITION += ' AND ( vlocity_cmt__IsActive__c = false ) ';
       }

       CONSTANT_DYNAMIC_QUERY =' Select ' + CONSTANT_FIELD_NAMES + 'from vlocity_cmt__OmniScript__c'+ CONSTANT_QUERY_CONDITION +' order by Name';
       return (List<vlocity_cmt__OmniScript__c>) Database.query(CONSTANT_DYNAMIC_QUERY);
    }
    
     /* Get  omniscript elements by omniscript Id  */
    @AuraEnabled
    public static Map<String,vlocity_cmt__Element__c>  getOmniscriptComponents(String omniscriptId){
        return new Map<String,vlocity_cmt__Element__c> ([SELECT Id, Name,vlocity_cmt__Order__c, vlocity_cmt__OmniScriptId__c, vlocity_cmt__ParentElementId__c, vlocity_cmt__Type__c,vlocity_cmt__ParentElementType__c, vlocity_cmt__ParentElementName__c from vlocity_cmt__Element__c  where vlocity_cmt__OmniScriptId__c =:omniscriptId order by  vlocity_cmt__Order__c ASC ]);
    }
     
       /***** 
       Merge selected omniscript element 
       Method  recievs list of configuration from the UI to merge omniscript elements
       *****/
    @AuraEnabled
    public static void copyeleemntfromOmniscript(List<String> listOfElementstoCopy,String sourceOSId, String destinationOSId, Map<String,String> mapOfElementNameAndParentId){
        
        if(mapOfElementNameAndParentId == null){
            mapOfElementNameAndParentId = new Map<String,String>();
        }
        
        List<vlocity_cmt__Element__c> listOfElementtoinsert = new List<vlocity_cmt__Element__c>();
        Map<String,vlocity_cmt__Element__c> mapOfElementsToInsert = new   Map<String,vlocity_cmt__Element__c>([SELECT Id, Name, vlocity_cmt__OmniScriptId__c, vlocity_cmt__ParentElementId__c, vlocity_cmt__Type__c, vlocity_cmt__Order__c, vlocity_cmt__PropertySet__c, vlocity_cmt__Level__c FROM vlocity_cmt__Element__c   where vlocity_cmt__OmniScriptId__c=:sourceOSId and Id in :listOfElementstoCopy ]);
        
        for(vlocity_cmt__Element__c destElement : [SELECT Id, Name FROM vlocity_cmt__Element__c WHERE vlocity_cmt__OmniScriptId__c =:destinationOSId] ){
            setOfNameOfDestinationOSElements.add(destElement.Name);
        }
      
         insertOsElementWithDependency(mapOfElementsToInsert, new Set<String>(), new Map<String,vlocity_cmt__Element__c>(), destinationOSId , mapOfElementNameAndParentId );
        
    }
    
    
    public static void insertOsElementWithDependency(Map<String,vlocity_cmt__Element__c> mapOfElementsToInsert, Set<String> setOfInsertedElementsId, Map<String,vlocity_cmt__Element__c> mapOfInsertedElements,String destinationOSId ,Map<String,String> mapOfElementNameAndParentId){
       
        Map<String,vlocity_cmt__Element__c> mapOfNewInsertedElements = new Map<String,vlocity_cmt__Element__c>();
        for(vlocity_cmt__Element__c elementToCopy :  mapOfElementsToInsert.values()){
            
            if(! mapOfInsertedElements.containsKey(elementToCopy.Name)){
               
                if(
                    elementToCopy.vlocity_cmt__ParentElementId__c == null || 
                    ( 
                    mapOfElementsToInsert.containsKey(elementToCopy.vlocity_cmt__ParentElementId__c) && mapOfInsertedElements.containsKey(mapOfElementsToInsert.get(elementToCopy.vlocity_cmt__ParentElementId__c).Name ))|| 
                    mapOfElementNameAndParentId.containsKey(elementToCopy.Name)
                  ){
                    vlocity_cmt__Element__c copiedElement  =   elementToCopy.clone(false, true, false, false);

                      while(setOfNameOfDestinationOSElements.contains(copiedElement.Name) || mapOfNewInsertedElements.containsKey(copiedElement.Name)){
                          copiedElement.Name += '_Copy'; 
                      }
                      
                    copiedElement.vlocity_cmt__OmniScriptId__c = destinationOSId;
                   
                    if(elementToCopy.vlocity_cmt__ParentElementId__c != null){
                        String parentId = mapOfElementNameAndParentId.containsKey(elementToCopy.Name) ? mapOfElementNameAndParentId.get(elementToCopy.Name) : mapOfInsertedElements.get(mapOfElementsToInsert.get(elementToCopy.vlocity_cmt__ParentElementId__c).Name).Id ;
                        copiedElement.vlocity_cmt__ParentElementId__c = parentId ;
                    }
                    
                    mapOfNewInsertedElements.put(copiedElement.Name, copiedElement);
                }
            }
        }
                
        if(!mapOfNewInsertedElements.Keyset().isEmpty() && CONSTANT_MAXIMUM_DML_COUNT !=0){
            insert mapOfNewInsertedElements.values();
            mapOfInsertedElements.putAll(mapOfNewInsertedElements);
            --CONSTANT_MAXIMUM_DML_COUNT;
        }
        
        if(mapOfElementsToInsert.Keyset().size() != mapOfInsertedElements.Keyset().size()){
          insertOsElementWithDependency(mapOfElementsToInsert,setOfInsertedElementsId,mapOfInsertedElements,destinationOSId,mapOfElementNameAndParentId);  
        }
        
    }
    
    
}