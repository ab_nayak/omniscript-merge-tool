({
    selectElement: function(component, event, helper) {


        let currentElement = event.getSource().get('v.name');
        let status = event.getSource().get('v.checked');
        let selectedElement = new Set(component.get('v.listOfElementSelected'));
        let mapOfElementVsParentElement = component.get('v.mapOfElementAndParentElement') || {}; // new Map();
        let listDependencySelectCheckbox = new Set(component.get('v.listDependencySelectCheckbox'));
        let hasDependencyError = component.get('v.dependencyError');


        if (status) {
            if (hasDependencyError) {
                Array.from(listDependencySelectCheckbox.values()).forEach(function(item) {
                    if (selectedElement.has(item)) {
                        selectedElement.delete(item);
                    }
                });

                listDependencySelectCheckbox.clear();
                helper.resetAllDependencyAttributes(component);

            }

            listDependencySelectCheckbox.clear();

            selectedElement.add(currentElement.Id);

            if (currentElement['vlocity_cmt__ParentElementId__c'] != undefined && currentElement['vlocity_cmt__ParentElementId__c'] != '') {
                if (!selectedElement.has(currentElement['vlocity_cmt__ParentElementId__c']) && !mapOfElementVsParentElement.hasOwnProperty(currentElement.Name)) {

                    component.set("v.dependencyError", true);
                    let sourceOmnisciptOriginalResponse = component.get('v.sourceOmnisciptOriginalResponse');
                    let destinationOmniscriptOriginalResponse = component.get('v.destinationOmniscriptOriginalResponse');
                    let listOfDestinationParentElementId = new Set(component.get('v.listOfDestinationParentElement'));


                    listDependencySelectCheckbox.add(currentElement.Id);
                    component.set("v.dependencyElement", currentElement);
                    component.set("v.dependencySourceElement", Object.values(sourceOmnisciptOriginalResponse).filter(function(item) {
                        return currentElement.vlocity_cmt__ParentElementId__c == item.Id;
                    }));
                    component.set("v.dependencyDestinationElement", Object.values(destinationOmniscriptOriginalResponse).filter(function(item) {
                        return listOfDestinationParentElementId.has(item.Id) || helper.allowedParentElementConfig.includes(item.vlocity_cmt__Type__c);
                    }));

                    component.set('v.isPreview', false);
                    event.getSource().set('v.checked', false);
                    event.getSource().set("v.class", ' dependency-error-CheckBox default-CheckBox ');

                } else {

                    event.getSource().set('v.checked', true);
                    var appEvent = $A.get("e.c:MS_NotifyDependencyCompletionEvent");
                    appEvent.setParams({ "elementId": currentElement.Id, 'elementListToCheck': Array.from(listDependencySelectCheckbox), 'invokeType': '' });
                    appEvent.fire();
                }

            }
        } else {

            helper.resetAllPreviewAttributes(component);
            
            //execute a logic to check no dependency on children upon unchecking parent
            let isdeleteElement = true;
            if (mapOfElementVsParentElement.hasOwnProperty(currentElement.Name)) {
                delete mapOfElementVsParentElement[currentElement.Name];
            } else {
                let childNodes = currentElement['ChildNode'] || [];
                if (childNodes) {
                    childNodes.forEach(function(elementsObj) {
                        if (selectedElement.has(elementsObj.Id) && !mapOfElementVsParentElement.hasOwnProperty(elementsObj.Name)) {
                            event.getSource().set('v.checked', true);
                            isdeleteElement = false;
                        }
                        
                    });
                }
            }

            if (isdeleteElement) {
                selectedElement.delete(currentElement.Id);
            } else {
                alert("You cannot uncheck this element as its child elements have already been selected. Please uncheck child elements first to continue with this operation");

            }
            
           
        }

        component.set("v.listDependencySelectCheckbox", Array.from(listDependencySelectCheckbox));
        component.set("v.listOfElementSelected", Array.from(selectedElement));
        
        if (!status) {
            var notifyWrapperComponent = $A.get("e.c:MS_NotifyWrapperComponentEvent");
            notifyWrapperComponent.fire();
        }
        

    },

    handleCheckboxChange: function(component, event, helper) {

        let elementSelected = event.getParam('elementListToCheck');
        let invokeType = event.getParam('invokeType');
        let sourceOmnisciptStructResponse = component.get('v.sourceOmnisciptStructResponse') || [];

        if (sourceOmnisciptStructResponse.length > 0) {
            sourceOmnisciptStructResponse.forEach(function(item) {
                if (elementSelected.includes(item.Id)) {
                    item['Checked'] = invokeType == 'de-select' ? false : true;
                    return false;
                }

            });

            component.set('v.sourceOmnisciptStructResponse', sourceOmnisciptStructResponse);
            helper.resetAllDependencyAttributes(component);
            helper.resetDestinationCheckbox(component);

        }


    },
    handleTargetCheckboxChange: function(component, event, helper) {

        var omniscriptpath = component.get('v.Omniscriptpath');
        if (omniscriptpath == 'Destination') {
            let elementSelectedId = event.getParam('targetElementId');

            let destinationOmnisciptStructResponse = component.get('v.sourceOmnisciptStructResponse');
            if (destinationOmnisciptStructResponse != undefined) {
                destinationOmnisciptStructResponse.forEach(function(item) {

                    if (elementSelectedId == item.Id) {
                        item['Checked'] = true;
                    } else {
                        item['Checked'] = false;
                    }

                });
                component.set('v.sourceOmnisciptStructResponse', destinationOmnisciptStructResponse);
                var parentElement = document.getElementById(elementSelectedId + 'dest');
                if (parentElement) {
                    parentElement.scrollIntoView(false);
                     
                }
            }

        }

    },

    manageElement: function(component, event, helper) {

        let currentElement = event.getSource().get('v.name');

        if (currentElement['vlocity_cmt__ParentElementId__c'] != undefined && currentElement['vlocity_cmt__ParentElementId__c'] != '') {
            component.set("v.dependencyError", true);
            let sourceOmnisciptOriginalResponse = component.get('v.sourceOmnisciptOriginalResponse');
            let destinationOmniscriptOriginalResponse = component.get('v.destinationOmniscriptOriginalResponse');
            let listOfDestinationParentElementId = new Set(component.get('v.listOfDestinationParentElement'));
            let mapOfElementAndParentElement = component.get('v.mapOfElementAndParentElement');
            let listOfElementSelected = component.get('v.listOfElementSelected');

            component.set("v.dependencyElement", currentElement);

            let isDestinationElementChoosed = mapOfElementAndParentElement[currentElement.Name] || "";

            if ((isDestinationElementChoosed == "" && !listOfElementSelected.includes(currentElement.vlocity_cmt__ParentElementId__c)) || isDestinationElementChoosed != "") {
                component.set("v.dependencySourceElement", Object.values(sourceOmnisciptOriginalResponse).filter(function(item) {
                    return currentElement.vlocity_cmt__ParentElementId__c == item.Id;
                }));
            }


            component.set('v.isPreview', false);
            component.set("v.dependencyDestinationElement", Object.values(destinationOmniscriptOriginalResponse).filter(function(item) {
                return listOfDestinationParentElementId.has(item.Id) || helper.allowedParentElementConfig.includes(item.vlocity_cmt__Type__c);
            }));

        }
    }


})