({
    "allowedParentElementConfig": ["Block", "Input Block", "Edit Block", "Conditional Block", "List Merge Action", "Try Catch Block", "Cache Block"],

    resetAllDependencyAttributes: function(component) {
        component.set("v.dependencyError", false);
        component.set("v.listDependencySelectCheckbox", []);
        component.set("v.dependencyElement", {});
        component.set("v.dependencySourceElement", []);
        component.set("v.dependencyDestinationElement", []);

    },
    resetAllPreviewAttributes: function(component) {
        component.set('v.isPreview', false);
        component.set("v.previewOmnisciptStructResponse", {});
        component.set("v.listOfPreviewParentElement", {});
    },
      resetDestinationCheckbox: function(component){
        let notifyDestElementHighlight = $A.get("e.c:MS_highlightDestinationElementEvent");
        notifyDestElementHighlight.setParams({ "elementId": '', 'targetElementId': "-" });
        notifyDestElementHighlight.fire()
    }
   
})