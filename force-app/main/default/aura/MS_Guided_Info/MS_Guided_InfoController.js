({

    doInit: function(component, event, helper) {

        let currentStep = component.get('v.currentStep');
        //  AllStageList
        let selectOmniscript = { "completed": false, "isCurrent": true, "StepCode": "Select_Omniscripts", "Name": "Choose Omniscripts" };
        let chooseElement = { "completed": false, "isCurrent": false, "StepCode": "Select_Elements", "Name": "Select Elements" };
        let preview = { "completed": false, "isCurrent": false, "StepCode": "Preview_Changes", "Name": "Preview Changes" };
        let finish = { "completed": false, "isCurrent": false, "StepCode": "Finish", "Name": "Finish" };
        let advanced = { "completed": false, "isCurrent": false, "StepCode": "Advanced", "Name": "Advanced" };

        let allStageList = [selectOmniscript, chooseElement, preview, finish, advanced];

        allStageList = helper.reArrangeStageHelper(allStageList, currentStep);

        component.set('v.AllStageList', allStageList);
    },



    handleNext: function(component, event, helper) {
        let listOfsteps = component.get('v.listOfSteps');
        let currentStep = component.get('v.currentStep');
        component.set('v.IsIntroductionStep', false);


        currentStep++;

        if (currentStep > listOfsteps.length) {
            currentStep = 1;
        }

        let allStageList = component.get('v.AllStageList');

        allStageList = helper.reArrangeStageHelper(allStageList, currentStep);

        component.set('v.AllStageList', allStageList);
        component.set('v.currentStep', currentStep);

    },

    handlePrevious: function(component, event, helper) {
        let listOfsteps = component.get('v.listOfSteps');
        let currentStep = component.get('v.currentStep');


        currentStep--;

        if (currentStep == 0) {
            currentStep = listOfsteps.length;
        }

        let allStageList = component.get('v.AllStageList');
        allStageList = helper.reArrangeStageHelper(allStageList, currentStep);

        component.set('v.AllStageList', allStageList);
        component.set('v.currentStep', currentStep);

    }



})