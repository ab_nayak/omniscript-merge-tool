({
    reArrangeStageHelper: function(allStageList, currentStageValue) {
        allStageList.forEach((element, index) => {
            if (index + 1 == currentStageValue) {
                element.isCurrent = true;
                element.completed = false;

            } else if (index + 1 < currentStageValue) {
                element.isCurrent = false;
                element.completed = true;

            } else {
                element.isCurrent = false;
                element.completed = false;

            }
        });

        return allStageList;


    }
})