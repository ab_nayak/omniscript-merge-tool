({
    doInit: function(component, event, helper) {
        helper.checkButtonEnableDisableHelper(component, event, helper);
        let destInputCmp = component.find("Id_input_Destination");
        if (destInputCmp) {
            destInputCmp.set("v.disabled", true);
        }
    },

    searchAllSourceOmniscript: function(component, event, helper) {
        let searchText = event.getSource().get("v.value") || "";

        if (searchText.trim().length < 4) {
            return;
        }

        // set required parameter to invoke apex
        let apexInvokeMethodName = 'getAllOmniscriptComponents';
        let setParams = { searchText: searchText, destinationQuery: false };
        let CallbackApexInvokeResponseHandler = function(responseResult) {
                component.set("v.sourceSearchResult", responseResult);

            }
            //invoke apex 
        helper.handleApexInvoke(component, CallbackApexInvokeResponseHandler, setParams, apexInvokeMethodName);

    },

    searchAllDestinationOmniscript: function(component, event, helper) {
        let searchText = event.getSource().get("v.value") || "";

        if (searchText.trim().length < 4) {
            return;
        }

        // set required parameter to invoke apex
        let apexInvokeMethodName = 'getAllOmniscriptComponents';
        let setParams = { searchText: searchText, destinationQuery: true };
        let CallbackApexInvokeResponseHandler = function(responseResult) {
            component.set("v.destinationSearchResult", responseResult);
        }

        //invoke apex 
        helper.handleApexInvoke(component, CallbackApexInvokeResponseHandler, setParams, apexInvokeMethodName);

    },

    loadSourceOs: function(component, event, helper) {
        component.set("v.sourceOmniscriptId", event.target.id);
        component.set("v.toggleSource", false);
        component.set("v.isLoading", true);


        component.set("v.sourceOmniscriptWrapper", component.get("v.sourceSearchResult").filter(function(currentElement) {
            return currentElement.Id == event.target.id;

        })[0]);

        let sourceOSResponse = component.get("v.sourceOmnisciptOriginalResponse");
        if (sourceOSResponse != null && sourceOSResponse != undefined) {
            helper.loadSourceOsHelper(component, event, helper);
        }
        let destInputCmp = component.find("Id_input_Destination");
        if (destInputCmp) {
            destInputCmp.set("v.disabled", false);
        }
        helper.resetAllPreviewAttributes(component);
        component.set("v.isLoading", false);

    },

    loadDestinationOS: function(component, event, helper) {
        component.set("v.isLoading", true);
        component.set("v.destOmniscriptId", event.target.id);
        component.set("v.toggleDestination", false);


        let sourceOSResponse = component.get("v.sourceOmnisciptOriginalResponse");

        component.set("v.destOmniscriptWrapper", component.get("v.destinationSearchResult").filter(function(currentElement) {
            return currentElement.Id == event.target.id;

        })[0]);

        if (sourceOSResponse == null || sourceOSResponse == undefined) {
            helper.loadSourceOsHelper(component, event, helper);
        }
        helper.resetAllPreviewAttributes(component);
        helper.loadDestinationOsHelper(component, event, helper);
    },

    // Event handler for any element is selected OR de-selected
    selectElement: function(component, event, helper) {

        let currentElement = event.getSource().get('v.name');
        let label = event.getSource().get('v.label');
        let status = event.getSource().get('v.checked');
        let selectedElement = new Set(component.get('v.listOfElementSelected'));
        let hasDependencyError = component.get('v.dependencyError');
        let mapOfElementVsParentElement = component.get('v.mapOfElementAndParentElement') || {}; //new Map();
        let dependencyElement = component.get('v.dependencyElement');
        let listDependencySelectCheckbox = new Set(component.get('v.listDependencySelectCheckbox'));

        if (status) {
            if (label != 'DependencyDestination' && label != 'DependencySource' && hasDependencyError) {

                Array.from(listDependencySelectCheckbox.values()).forEach(function(item) {
                    if (selectedElement.has(item)) {
                        selectedElement.delete(item);
                    }
                });

                listDependencySelectCheckbox.clear();
                helper.resetAllDependencyAttributes(component);
                helper.resetDestinationCheckbox(component);



            }
            if (label == 'DependencyDestination') {

                mapOfElementVsParentElement[dependencyElement.Name] = currentElement.Id;
                listDependencySelectCheckbox.add(currentElement.Id);

                let appEvent = $A.get("e.c:MS_NotifyDependencyCompletionEvent");
                appEvent.setParams({ "elementId": currentElement.Id, 'elementListToCheck': Array.from(listDependencySelectCheckbox), 'invokeType': '' });
                appEvent.fire();

                listDependencySelectCheckbox.clear();

            } else {

                selectedElement.add(currentElement.Id);
                if (label == 'DependencySource') {
                    component.set("v.dependencyError", false);
                    selectedElement.add(currentElement.Id);
                    listDependencySelectCheckbox.add(currentElement.Id);
                    if (mapOfElementVsParentElement.hasOwnProperty(dependencyElement.Name)) {
                        delete mapOfElementVsParentElement[dependencyElement.Name];
                    }
                }

                if (currentElement['vlocity_cmt__ParentElementId__c'] != undefined && currentElement['vlocity_cmt__ParentElementId__c'] != '') {
                    if (!selectedElement.has(currentElement['vlocity_cmt__ParentElementId__c']) && !mapOfElementVsParentElement.hasOwnProperty(currentElement.Name)) {

                        component.set("v.dependencyError", true);
                        let sourceOmnisciptOriginalResponse = component.get('v.sourceOmnisciptOriginalResponse');
                        let destinationOmniscriptOriginalResponse = component.get('v.destinationOmniscriptOriginalResponse');
                        let listOfDestinationParentElementId = new Set(component.get('v.listOfDestinationParentElement'));

                        listDependencySelectCheckbox.add(currentElement.Id);


                        component.set("v.dependencyElement", currentElement);
                        component.set("v.dependencySourceElement", Object.values(sourceOmnisciptOriginalResponse).filter(function(item) {
                            return currentElement.vlocity_cmt__ParentElementId__c == item.Id;
                        }));

                        component.set("v.dependencyDestinationElement", Object.values(destinationOmniscriptOriginalResponse).filter(function(item) {
                            return listOfDestinationParentElementId.has(item.Id);
                        }));

                        event.getSource().set('v.checked', false);
                        component.set('v.isPreview', false);


                    } else {
                        listDependencySelectCheckbox.clear();
                    }
                } else {

                    selectedElement.add(currentElement.Id);
                    event.getSource().set('v.checked', true);

                    let appEvent = $A.get("e.c:MS_NotifyDependencyCompletionEvent");
                    appEvent.setParams({ "elementId": currentElement.Id, 'elementListToCheck': Array.from(listDependencySelectCheckbox), 'invokeType': '' });
                    appEvent.fire();
                    listDependencySelectCheckbox.clear();


                }


            }
        } else {

            helper.resetAllPreviewAttributes(component);

            let isdeleteElement = true;
            if (mapOfElementVsParentElement.hasOwnProperty(currentElement.Name)) {
                delete mapOfElementVsParentElement[currentElement.Name];
            } else {
                let childNodes = currentElement['ChildNode'] || [];

                if (childNodes) {
                    childNodes.forEach(function(elementsObj) {
                        if (selectedElement.has(elementsObj.Id) && !mapOfElementVsParentElement.hasOwnProperty(elementsObj.Name)) {
                            event.getSource().set('v.checked', true);
                            isdeleteElement = false;
                        }

                    });
                }
            }

            if (isdeleteElement) {
                selectedElement.delete(currentElement.Id);
            } else {

                var fireInfoModal = $A.get("e.force:showToast");

                fireInfoModal.setParams({
                    "duration": 6000,
                    "type": "info",
                    "mode": "pester",
                    "title": "You cannot uncheck this element",
                    "message": "Child elements have been already selected. Please uncheck child elements first to continue with this operation"

                });
                fireInfoModal.fire();

            }

        }

        component.set("v.mapOfElementAndParentElement", {});
        component.set("v.mapOfElementAndParentElement", mapOfElementVsParentElement);
        component.set("v.listDependencySelectCheckbox", Array.from(listDependencySelectCheckbox));
        component.set("v.listOfElementSelected", Array.from(selectedElement));

        helper.checkButtonEnableDisableHelper(component, event, helper);

    },

    // Save copied elements to other OS/Integration Procedures
    CopyAndMergeElementsController: function(component, event, helper) {
        component.set("v.isLoading", true);

        let listOfElementstoCopy = component.get("v.listOfElementSelected");
        let destOSId = component.get("v.destOmniscriptId");
        let listDependencySelectCheckbox = new Set(component.get('v.listDependencySelectCheckbox'));

        event.getSource().set("v.disabled", true);
        event.preventDefault();

        if (listOfElementstoCopy != null && listOfElementstoCopy.length > 0 && listDependencySelectCheckbox.size > 0) {
            listOfElementstoCopy.forEach(function(elementToCopy) {
                if (listDependencySelectCheckbox.has(elementToCopy)) {
                    listOfElementstoCopy.splice(listOfElementstoCopy.indexOf(elementToCopy), 1);
                }

            });
        }

        if (destOSId.trim().length < 15 && (listOfElementstoCopy == null || listOfElementstoCopy.length == 0)) {
            helper.checkButtonEnableDisableHelper(component, event, helper);
            return;
        }


        // set required parameter to invoke apex
        let apexInvokeMethodName = 'copyeleemntfromOmniscript';
        let setParams = {
            listOfElementstoCopy: listOfElementstoCopy,
            sourceOSId: component.get("v.sourceOmniscriptId"),
            destinationOSId: destOSId,
            mapOfElementNameAndParentId: component.get("v.mapOfElementAndParentElement")
        };

        let CallbackApexInvokeResponseHandler = function(responseResult) {

                component.set("v.copiedSuccess", true);
                component.set("v.mapOfElementAndParentElement", {});
                component.set("v.listOfElementSelected", []);
                component.set("v.listOfSourceParentElement", []);
                component.set('v.isPreview', false);

                helper.resetAllDependencyAttributes(component);
                helper.resetAllPreviewAttributes(component);


                /* Post Merging elememnts , these two step will re- arrange source and destination omniscript elements */
                helper.reStructureOmniscriptElement(component, event, helper, JSON.parse(JSON.stringify(component.get("v.sourceOmnisciptOriginalResponse"))), 'sourceOmnisciptStructResponse', 'listOfSourceParentElement');
                helper.loadDestinationOsHelper(component, event, helper);
                component.set("v.isLoading", false);

                let successMessageModal = $A.get("e.force:showToast");
                successMessageModal.setParams({
                    "type": "success",
                    "title": "Updated!",
                    "message": "Elements  have been copied successfully."
                });
                successMessageModal.fire();

            }
            //invoke apex 
        helper.handleApexInvoke(component, CallbackApexInvokeResponseHandler, setParams, apexInvokeMethodName);

    },

    handleCheckboxChange: function(component, event, helper) {

        let elementSelected = event.getParam('elementListToCheck');
        let invokeType = event.getParam('invokeType');
        let sourceOmnisciptStructResponse = component.get('v.sourceOmnisciptStructResponse');

        sourceOmnisciptStructResponse.forEach(function(item) {
            if (elementSelected.includes(item.Id)) {
                item['Checked'] = invokeType == 'de-select' ? false : true;
                return false;
            }

        });

        component.set('v.sourceOmnisciptStructResponse', sourceOmnisciptStructResponse);
        helper.resetAllDependencyAttributes(component);
        helper.resetDestinationCheckbox(component);



    },

    // need to review its functionality
    handleTargetElementHighlightEvent: function(component, event, helper) {

        let omniscriptpath = component.get('v.Omniscriptpath');
        if (omniscriptpath == 'Destination') {

            let elementSelectedId = event.getParam('targetElementId');
            let destinationOmnisciptStructResponse = component.get('v.destinationOmniscriptStructResponse');

            destinationOmnisciptStructResponse.forEach(function(item) {

                if (elementSelectedId == item.Id) {
                    item['Checked'] = true;
                } else {
                    item['Checked'] = false;
                }

            });
            component.set('v.destinationOmniscriptStructResponse', destinationOmnisciptStructResponse);
            var parentElement = document.getElementById(elementSelectedId + 'dest');
            if (parentElement) {
                parentElement.scrollIntoView(false);
            }
        }


    },

    handleOnHoverElement: function(component, event, helper) {

        component.set('v.Omniscriptpath', 'Destination');
        let notifyDestElementHighlight = $A.get("e.c:MS_highlightDestinationElementEvent");
        notifyDestElementHighlight.setParams({ "elementId": '', 'targetElementId': event.target.id, 'actionType': 'Select' });
        notifyDestElementHighlight.fire();

    },

    handlePreviewController: function(component, event, helper) {

        let isPreview = component.get('v.isPreview');
        component.set("v.isLoading", true);

        component.set('v.isPreview', !isPreview);
        component.set("v.previewOmnisciptStructResponse", {});
        component.set("v.listOfPreviewParentElement", {});

        if (!isPreview) {

            let listDependencySelectCheckbox = new Set(component.get('v.listDependencySelectCheckbox'));
            let destOriginalResponse = component.get('v.destinationOmniscriptOriginalResponse');
            let sourceOmniOriginalResponse = component.get('v.sourceOmnisciptOriginalResponse');
            let sourceElementAndDestParent = component.get('v.mapOfElementAndParentElement');
            let listOfElementSelected = component.get('v.listOfElementSelected');

            let listOfElementToInclude = {};
            let selectedElement = new Set(listOfElementSelected);

            Array.from(listDependencySelectCheckbox.values()).forEach(function(item) {
                if (selectedElement.has(item)) {
                    selectedElement.delete(item);
                }
            });

            listDependencySelectCheckbox.clear();

            helper.resetAllDependencyAttributes(component);

            component.set("v.listOfElementSelected", Array.from(selectedElement));
            listOfElementSelected = component.get('v.listOfElementSelected');

            Object.values(sourceOmniOriginalResponse).forEach(function(element) {
                let elementCopy = JSON.parse(JSON.stringify(element));
                if (sourceElementAndDestParent != null && sourceElementAndDestParent != undefined && sourceElementAndDestParent.hasOwnProperty(elementCopy.Name)) {

                    elementCopy.vlocity_cmt__ParentElementId__c = sourceElementAndDestParent[elementCopy.Name];
                    elementCopy.isCopied = true;
                }
                if (listOfElementSelected.includes(elementCopy.Id)) {
                    elementCopy.isCopied = true;
                    listOfElementToInclude[elementCopy.Id] = elementCopy;
                }

            });

            Object.assign(listOfElementToInclude, destOriginalResponse);
            component.set("v.overflow_PVW_Count", Object.keys(listOfElementToInclude).length > 25);

            helper.reStructureOmniscriptElement(component, event, helper, JSON.parse(JSON.stringify(listOfElementToInclude)), 'previewOmnisciptStructResponse', 'listOfPreviewParentElement');


        }

        component.set("v.isLoading", false);
        helper.resetDestinationCheckbox(component);



    },

    hideSourceResult: function(component, event, helper) {
        console.log('hideSourceResult method is called');
        let attributename = event.target.id;
        component.set('v.' + attributename, false);
    },

    handleToggleAction: function(component, event, helper) {
        let attrName = event.getSource().get("v.name");
        component.set("v." + attrName, !component.get("v." + attrName));

    },

    handleScrollTopAction: function(component, event, helper) {
        let blockId = event.getSource().get("v.name");
        let scrollableElement = document.getElementById(blockId);
        if (scrollableElement) {
            scrollableElement.scrollTop = 0;
        }

    },
    handleNotifyWrapperComponent: function(component, event, helper) {
        console.log(" notify wrapper event is recieved");
        helper.checkButtonEnableDisableHelper(component, event, helper);

    },

    HandleHelpModalBox: function(component, event, helper) {
        let stepIndexValue = parseInt(event.getSource().get("v.name")) || 0;

        let IsIntroductionStep = stepIndexValue == 0 ? true : false;
        var modalBody;
        $A.createComponent("c:MS_Guided_Info", { "currentStep": stepIndexValue, "IsIntroductionStep": IsIntroductionStep },
            function(content, status) {
                if (status === "SUCCESS") {
                    modalBody = content;
                    component.find('IdHelpPopUp').showCustomModal({
                        header: "Get Started with Omniscript Merge Tool",
                        body: modalBody,
                        showCloseButton: true,
                        cssClass: "slds-modal__container, modal-body ,slds-modal__footer ,slds-modal__close, cMS_ElementMergeWrapper",
                        closeCallback: function() {

                        }
                    })
                }
            });
    }


})