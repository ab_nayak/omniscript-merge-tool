({
    loadSourceOsHelper : function(component, event, helper) {
        
        let omniscriptId = component.get("v.sourceOmniscriptId");
        if (omniscriptId.trim().length < 15) {
            return;
        }
        
        helper.resetAllDependencyAttributes(component);
        
        
        component.set("v.listOfElementSelected", []);
        component.set("v.sourceOmnisciptOriginalResponse", {});
        component.set("v.listOfSourceParentElement", []);
        component.set("v.sourceOmnisciptStructResponse", []);
        
        // Index out of bound will never occures due to "sourceSearchResult" attribute must contain a user choosed record.
        
        // set required parameter to invoke apex
        let apexInvokeMethodName = 'getOmniscriptComponents';
        let setParams = { omniscriptId: omniscriptId };
        let CallbackApexInvokeResponseHandler = function(responseResult) {
            
            let sourceOmnisciptOriginalResponse = JSON.parse(JSON.stringify(responseResult));
            
            component.set("v.sourceOmnisciptOriginalResponse", sourceOmnisciptOriginalResponse);
            component.set("v.overflow_SRC_Count", Object.keys(sourceOmnisciptOriginalResponse).length >25);
            
            helper.reStructureOmniscriptElement(component, event, helper, JSON.parse(JSON.stringify(responseResult)), 'sourceOmnisciptStructResponse', 'listOfSourceParentElement');
            helper.checkButtonEnableDisableHelper(component, event, helper) ;
            component.set("v.isLoading", false);
        }
        
        //invoke apex 
        helper.handleApexInvoke(component, CallbackApexInvokeResponseHandler, setParams, apexInvokeMethodName);

        
    },
     
    loadDestinationOsHelper: function(component, event, helper) {

        let selectedElement = new Set(component.get('v.listOfElementSelected'));
        
        let destCompRef = component.find("AuraId_dest-cmp");
        if (destCompRef != undefined || destCompRef != null) {
            if (Array.isArray(destCompRef)) {
                destCompRef.forEach(function(cmpRef) {
                    cmpRef.destroy();
                });
            } else {
                destCompRef.destroy();
            }

        }

        this.resetAllDependencyAttributes(component);
        
        component.set("v.listOfElementSelected", []);
        component.set("v.destinationOmniscriptStructResponse", []);

        // set required parameter to invoke apex
        let apexInvokeMethodName = 'getOmniscriptComponents';

        let destomniscriptId = component.get("v.destOmniscriptId");
        if (destomniscriptId.trim().length < 15) {
            return;
        }
        let setParams = { omniscriptId: destomniscriptId };

        let CallbackApexInvokeResponseHandler = function(responseResult) {
            component.set("v.isLoading", false);
            component.set("v.destinationOmniscriptOriginalResponse", responseResult);
            component.set("v.overflow_DSNTN_Count", Object.keys(responseResult).length >25);
            
            helper.reStructureOmniscriptElement(component, event, helper, JSON.parse(JSON.stringify(responseResult)), 'destinationOmniscriptStructResponse', 'listOfDestinationParentElement');
            helper.checkButtonEnableDisableHelper(component, event, helper) ;
            
            let appEvent = $A.get("e.c:MS_NotifyDependencyCompletionEvent");
            appEvent.setParams({ "elementId":'', 'elementListToCheck': Array.from(selectedElement), 'invokeType': 'de-select' });
            appEvent.fire();
        }

        //invoke apex 
        helper.handleApexInvoke(component, CallbackApexInvokeResponseHandler, setParams, apexInvokeMethodName);


    },

    handleApexInvoke: function(component, callBackName, params, invokeMethodName) {
        let action = component.get("c." + invokeMethodName);
        if (params) {
            action.setParams(params);
        }
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                callBackName(response.getReturnValue());
            } else {
                component.set("v.isLoading", false);
                this.errorHandlerHelper(response.getError());
            }
        });
        $A.enqueueAction(action);
    },


    errorHandlerHelper: function(errors) {
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    },

    reStructureOmniscriptElement: function(component, event, Helper, response, attributName, parentMappingAttributeName) {
        let mapOfParentElements = new Map();
        let mapOfAllElements = new Map();
        mapOfAllElements = response;
        let constructedElements = [];

        Object.values(mapOfAllElements).forEach(function(value) {
            if (value['vlocity_cmt__ParentElementId__c'] != undefined && value['vlocity_cmt__ParentElementId__c'] != '') {
                if (mapOfParentElements.has(value.vlocity_cmt__ParentElementId__c)) {
                    mapOfParentElements.get(value.vlocity_cmt__ParentElementId__c).add(value.Id);
                } else {
                    mapOfParentElements.set(value.vlocity_cmt__ParentElementId__c, new Set([value.Id]));
                }

            }
        });

        component.set("v." + parentMappingAttributeName, Array.from(mapOfParentElements.keys()));


        function constructElementHeirarchy(elementsMap) {
            Object.values(elementsMap).forEach(function(value) {
                value['Checked'] = false;
                if (value['vlocity_cmt__ParentElementId__c'] != undefined && value['vlocity_cmt__ParentElementId__c'] != '') {
                    if (!mapOfParentElements.has(value.Id)) {
                        if (elementsMap[value.vlocity_cmt__ParentElementId__c].hasOwnProperty('ChildNode')) {
                            elementsMap[value.vlocity_cmt__ParentElementId__c].ChildNode.push(value);

                        } else {
                            elementsMap[value.vlocity_cmt__ParentElementId__c].ChildNode = [value];
                        }

                        if (mapOfParentElements.has(value.vlocity_cmt__ParentElementId__c)) {
                            mapOfParentElements.get(value.vlocity_cmt__ParentElementId__c).delete(value.Id);
                            if (mapOfParentElements.get(value.vlocity_cmt__ParentElementId__c).size == 0) {
                                mapOfParentElements.delete(value.vlocity_cmt__ParentElementId__c);
                            }
                        }

                        delete elementsMap[value.Id];
                    }
                }
            });

            if (mapOfParentElements.size != 0) {
                constructElementHeirarchy(mapOfAllElements);
            }

        }

        constructElementHeirarchy(mapOfAllElements);
        component.set("v." + attributName, Object.values(mapOfAllElements));


    },

    reStructurePreviewOmniscriptElement: function(component, event, Helper, response, attributName, parentMappingAttributeName) {
        let mapOfParentElements = new Map();
        let mapOfAllElements = new Map();
        mapOfAllElements = response;
        let constructedElements = [];

        Object.values(mapOfAllElements).forEach(function(value) {
            if (value['vlocity_cmt__ParentElementId__c'] != undefined && value['vlocity_cmt__ParentElementId__c'] != '') {
                if (mapOfParentElements.has(value.vlocity_cmt__ParentElementId__c)) {
                    mapOfParentElements.get(value.vlocity_cmt__ParentElementId__c).add(value.Id);
                } else {
                    mapOfParentElements.set(value.vlocity_cmt__ParentElementId__c, new Set([value.Id]));
                }

            }
        });

        component.set("v." + parentMappingAttributeName, Array.from(mapOfParentElements.keys()));


        function constructElementHeirarchy(elementsMap) {
            Object.values(elementsMap).forEach(function(value) {
                value['Checked'] = false;
                if (value['vlocity_cmt__ParentElementId__c'] != undefined && value['vlocity_cmt__ParentElementId__c'] != '') {
                    if (!mapOfParentElements.has(value.Id)) {
                        if (elementsMap[value.vlocity_cmt__ParentElementId__c].hasOwnProperty('ChildNode')) {
                            elementsMap[value.vlocity_cmt__ParentElementId__c].ChildNode.push(value);

                        } else {
                            elementsMap[value.vlocity_cmt__ParentElementId__c].ChildNode = [value];
                        }

                        if (mapOfParentElements.has(value.vlocity_cmt__ParentElementId__c)) {
                            mapOfParentElements.get(value.vlocity_cmt__ParentElementId__c).delete(value.Id);
                            if (mapOfParentElements.get(value.vlocity_cmt__ParentElementId__c).size == 0) {
                                mapOfParentElements.delete(value.vlocity_cmt__ParentElementId__c);
                            }
                        }

                        delete elementsMap[value.Id];
                    }
                }
            });

            if (mapOfParentElements.size != 0) {
                constructElementHeirarchy(mapOfAllElements);
            }

        }

        constructElementHeirarchy(mapOfAllElements);
        component.set("v." + attributName, Object.values(mapOfAllElements));


    },

    checkButtonEnableDisableHelper : function(component ,event ,helper){
        let listOfElementSelected =  component.get("v.listOfElementSelected") ||[];
        component.find("Id_ActionButton").forEach(function(auraButtonCmp){
            auraButtonCmp.set("v.disabled",listOfElementSelected.length ==0);
        });
      
    },
    
    resetAllDependencyAttributes: function(component) {
        component.set("v.dependencyError", false);
        component.set("v.listDependencySelectCheckbox", []);
        component.set("v.dependencyElement", {});
        component.set("v.dependencySourceElement", []);
        component.set("v.dependencyDestinationElement", []);

    },
    resetAllPreviewAttributes: function(component) {
        component.set('v.isPreview', false);
        component.set("v.previewOmnisciptStructResponse", {});
        component.set("v.listOfPreviewParentElement", {});
    },
   
    resetDestinationCheckbox: function(component){
        let notifyDestElementHighlight = $A.get("e.c:MS_highlightDestinationElementEvent");
        notifyDestElementHighlight.setParams({ "elementId": '', 'targetElementId': "-" });
        notifyDestElementHighlight.fire()
    }
    


})